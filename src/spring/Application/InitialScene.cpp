#include "..\..\..\include\spring\Application\InitialScene.h"
#include <qapplication.h>
#include <iostream>

namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		createGUI();

		QObject::connect(sayHelloPushButton, SIGNAL(released()), this, SLOT(mf_sayHelloPushButton()));
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{
	}

	void InitialScene::createGUI()
	{
		m_uMainWindow->resize(300, 100);

		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		horizontalLayout = new QHBoxLayout(centralWidget);
		horizontalLayout->setSpacing(6);
		horizontalLayout->setContentsMargins(11, 11, 11, 11);
		horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(6);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 0, 0, 1, 1);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer_2, 0, 2, 1, 1);

		nameLineEdit = new QLineEdit(centralWidget);
		nameLineEdit->setObjectName(QStringLiteral("nameLineEdit"));

		gridLayout->addWidget(nameLineEdit, 0, 1, 1, 1, Qt::AlignHCenter | Qt::AlignVCenter);

		sayHelloPushButton = new QPushButton(centralWidget);
		sayHelloPushButton->setObjectName(QStringLiteral("sayHelloPushButton"));
		QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		sizePolicy.setHorizontalStretch(0);
		sizePolicy.setVerticalStretch(0);
		sizePolicy.setHeightForWidth(sayHelloPushButton->sizePolicy().hasHeightForWidth());
		sayHelloPushButton->setSizePolicy(sizePolicy);
		sayHelloPushButton->setLayoutDirection(Qt::LeftToRight);

		gridLayout->addWidget(sayHelloPushButton, 1, 1, 1, 1, Qt::AlignHCenter | Qt::AlignVCenter);

		horizontalLayout->addLayout(gridLayout);

		m_uMainWindow.get()->setCentralWidget(centralWidget);
		menuBar = new QMenuBar(m_uMainWindow.get());
		menuBar->setObjectName(QStringLiteral("menuBar"));
		menuBar->setGeometry(QRect(0, 0, 426, 26));
		m_uMainWindow.get()->setMenuBar(menuBar);
		statusBar = new QStatusBar(m_uMainWindow.get());
		statusBar->setObjectName(QStringLiteral("statusBar"));
		m_uMainWindow.get()->setStatusBar(statusBar);

		m_uMainWindow.get()->setWindowTitle(QApplication::translate("MainWindow", "Say Hello", Q_NULLPTR));
		sayHelloPushButton->setText(QApplication::translate("MainWindow", "Say Hello !", Q_NULLPTR));
		nameLineEdit->setText(boost::any_cast<const QString>(m_TransientDataCollection.find("InputNameKey")->second));
	}

	void InitialScene::mf_sayHelloPushButton()
	{
		const QString inputName = nameLineEdit->text();
		m_TransientDataCollection.erase("InputNameKey");
		m_TransientDataCollection.emplace("InputNameKey", inputName);

		const std::string c_szNextSceneName = "BaseScene";
		emit SceneChange(c_szNextSceneName);
	}
}

