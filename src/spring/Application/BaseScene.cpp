#include "..\..\..\include\spring\Application\BaseScene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{
		createGUI();

		QObject::connect(backPushButton, SIGNAL(released()), this, SLOT(mf_backPushButton()));
	}

	void BaseScene::createGUI()
	{
		m_uMainWindow->setWindowTitle(QString("Welcome !"));

		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		horizontalLayout = new QHBoxLayout(centralWidget);
		horizontalLayout->setSpacing(6);
		horizontalLayout->setContentsMargins(11, 11, 11, 11);
		horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(6);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		labelHello = new QLabel(centralWidget);
		labelHello->setObjectName(QStringLiteral("labelHello"));

		gridLayout->addWidget(labelHello, 1, 1, 1, 1, Qt::AlignHCenter | Qt::AlignVCenter);

		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer_2, 0, 1, 1, 1);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer_2, 1, 2, 1, 1);

		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer, 3, 1, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 1, 0, 1, 1);

		backPushButton = new QPushButton(centralWidget);
		backPushButton->setObjectName(QStringLiteral("backPushButton"));

		gridLayout->addWidget(backPushButton, 2, 1, 1, 1);


		horizontalLayout->addLayout(gridLayout);

		m_uMainWindow.get()->setCentralWidget(centralWidget);
		menuBar = new QMenuBar(m_uMainWindow.get());
		menuBar->setObjectName(QStringLiteral("menuBar"));
		menuBar->setGeometry(QRect(0, 0, 426, 26));
		m_uMainWindow.get()->setMenuBar(menuBar);
		statusBar = new QStatusBar(m_uMainWindow.get());
		statusBar->setObjectName(QStringLiteral("statusBar"));
		m_uMainWindow.get()->setStatusBar(statusBar);

		labelHello->setText(QString("Hello ") + boost::any_cast<QString>(m_TransientDataCollection.find("InputNameKey")->second) + QString(" !"));
		backPushButton->setText(QApplication::translate("MainWindow", "Back", Q_NULLPTR));
	}

	void BaseScene::release()
	{
		delete centralWidget;
	}
	
	BaseScene::~BaseScene()
	{
	}

	void BaseScene::mf_backPushButton()
	{
		const std::string c_szNextSceneName = "InitialScene";
		emit SceneChange(c_szNextSceneName);
	}
}
