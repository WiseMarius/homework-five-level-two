#pragma once
#include <spring\Framework\IScene.h>

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>
#include <QtWidgets/QPushButton>

namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT

	public:

		explicit BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~BaseScene();

	private:
		QWidget *centralWidget;
		QHBoxLayout *horizontalLayout;
		QGridLayout *gridLayout;
		QLabel *labelHello;
		QSpacerItem *horizontalSpacer_2;
		QSpacerItem *verticalSpacer;
		QSpacerItem *horizontalSpacer;
		QSpacerItem *verticalSpacer_2;
		QMenuBar *menuBar;
		QStatusBar *statusBar;
		QPushButton *backPushButton;
		
		void createGUI();

		private slots:
		void mf_backPushButton();
	};

}
